# Copyright 2023 Cedric Bellegarde <cedric.bellegarde@adishatz.org>

from dozed_graph.plot import EventPlotter

import matplotlib.pyplot as plt

class Graph:
    def __init__(self, file):
        self.file = file
        
    def make(self):
        with open(self.file) as f:
            lines = f.readlines()
            self.__make(lines)
            
    def __make(self, data):
        plotter = EventPlotter()
        for line in data:
            line = line.replace('\n', '')
            if line == "": continue
            plotter.analyze(line)
        plotter.draw()
