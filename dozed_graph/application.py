# Copyright 2023 Cedric Bellegarde <cedric.bellegarde@adishatz.org>

from dozed_graph.graph import Graph

class Application:
    def __init__(self, argv):
        self.argv = argv
        
    def run(self):
        if len(self.argv) == 1:
                print("Usage:")
                print("dozed-graph history.dat")
        else:
            graph = Graph(self.argv[1])
            graph.make()
        return 0
