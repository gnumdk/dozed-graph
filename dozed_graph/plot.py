# Copyright 2023 Cedric Bellegarde <cedric.bellegarde@adishatz.org>

import matplotlib.pyplot as plt
import numpy as np
from datetime import datetime

class EventPlotter:
    def __init__(self):
        self.__colors = {}
        self.__alarms = {}
        self.__alarm_ringed = []
        self.__maintenance = {}
        self.__maintenance_finished = []
        self.__battery = {}
        self.__sleeping = {}
        self.__y = 100
        
    def analyze(self, line):
        try:
            split = line.split(":")
            timestamp = int(split[0])
            event_type = split[1]
            args = split[2:]
            if event_type == "maintenance":
                self.__analyze_maintenance(timestamp, args)
            elif event_type == "alarm":
                self.__analyze_alarm(timestamp, args)
            elif event_type == "system":
                self.__analyze_system(timestamp, args)
        except Exception as e:
            print("EventPlotter::analyze():", e)

    def draw(self):
        self.__draw_battery()
        self.__draw_sleeping()
        self.__draw_alarms()
        self.__draw_maintenance()
        plt.xlabel('Time')
        plt.ylabel('Battery percent')
        plt.title('Dozing history')
        ax = plt.gca()
        line = ax.lines[0]
        tick_locs = line.get_xdata()
        tick_lbls = []
        day = None
        for timestamp in tick_locs:
            _day = datetime.fromtimestamp(timestamp).strftime('%d')
            if day == _day:
                tick_lbls.append(
                    datetime.fromtimestamp(timestamp).strftime('%H:%M')
                )
            else:
                day = _day
                tick_lbls.append(
                    datetime.fromtimestamp(timestamp).strftime(
                        '%H:%M\n%m-%d')
                )
        plt.xticks(tick_locs, tick_lbls)
        plt.legend()
        plt.show()

    def __analyze_maintenance(self, timestamp, args):
        event = args[0]
        if event == "registered":
            if args[1] not in self.__maintenance.keys():
                self.__maintenance[args[1]] = []
            self.__maintenance[args[1]].append(timestamp)
        elif event == "finished":
            self.__maintenance_finished.append(timestamp)

    def __analyze_alarm(self, timestamp, args):
        event = args[0]
        if event == "add":
            if args[1] not in self.__alarms.keys():
                self.__alarms[args[1]] = []
            self.__alarms[args[1]].append(timestamp)
        elif event == "ringed":
            self.__alarm_ringed.append(timestamp)
        
    def __analyze_system(self, timestamp, args):
        event = args[0]
        if event == "battery":
            keys = list(self.__battery.keys())
            if len(keys) == 0 or timestamp - keys[-1] > 1800: 
                self.__battery[timestamp] = float(args[1])
        elif event == "suspend":
            self.__sleeping[timestamp] = None
        elif event == "resume":
            for key in self.__sleeping.keys():
                if self.__sleeping[key] is None:
                    self.__sleeping[key] = timestamp

    def __draw_battery(self):
        x = []
        y = []
        day = None
        for timestamp in sorted(self.__battery.keys()):
            x.append(timestamp)
            y.append(self.__battery[timestamp])
        plt.plot(x, y, label="Battery")

    def __draw_alarms(self):
        for app_id in self.__alarms.keys():
            self.__y -= 2
            for timestamp in self.__alarms[app_id]:
                self.__draw_dot(timestamp, self.__y, "^", "%s alarm" % app_id)

        self.__y -= 2
        for timestamp in self.__alarm_ringed:
            self.__draw_dot(timestamp, self.__y, "v", "Alarm ringed")
        self.__y -= 3

    def __draw_sleeping(self):
        first = True
        for suspend in self.__sleeping.keys():
            resume = self.__sleeping[suspend]
            x = [suspend, resume]
            y = [100, 100]
            if first:
                first = False
                plt.plot(x, y, color="black", label="Sleeping")
            else:
                plt.plot(x, y, color="black")

    def __draw_maintenance(self):
        for app_id in self.__maintenance.keys():
            self.__y -= 2
            for timestamp in self.__maintenance[app_id]:
                self.__draw_dot(timestamp, self.__y, ">", "%s maintenance" % app_id)
            
        self.__y -= 2
        for timestamp in self.__maintenance_finished:
            self.__draw_dot(timestamp, self.__y, "s", "Maintenance finished")
        self.__y -= 3

    def __draw_dot(self, x, y, fig, label):
        if label in self.__colors.keys():
            print(label)
            color = self.__colors[label]
            for line in plt.plot(x, y, fig):
                line.set_color(color)
        else:
            line = plt.plot(x, y, fig, label=label)[0]
            self.__colors[label] = line.get_color()
